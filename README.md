# Fold.ERP [confidential]

To mount project hidden volumes:<br />

1a. get server-mounter binary - version 1.56.201806111042<br /> 
$ folderp <SERVER_IMAGE><br />
1b. Follow the steps in the wizard (see server data)<br />

2a. get client-mounter - version 0.7.201803011120 <br />
$ java -jar folderp-gui.jar <br />
2b. Follow the steps in the wizard (see client data)<br />

**Server data**:<br />
-----BEGIN PUBLIC KEY-----<br />
MIGeMA0GCSqGSIb3DQEBAQUAA4GMADCBiAKBgHqFaTq9j8/4H3Igksr0orKLTeMr
ZbQEptNlUQNKG5A39kVC1EdPGJYlXksZNHl6tRwg3V6CBpz/e0Aj9ZCwB4E9Wl9o
wKEGfNKiXn5yIOsH1crECsArTpyQpxNnP3UXVmcRe4Anhd7eQczzPn+ZxMKEIVUG
DBI24IwR1GamGXmvAgMBAAE=<br />
-----END PUBLIC KEY-----<br />

Server requirements:<br />
1. python3.6
2. pipenv


**Client data**:<br />
-----BEGIN PUBLIC KEY-----<br />
MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC+RCMHvTn7x3aFVre1iJcrbJDl
tCqaYNta1lzj5RsFcf8mOP55E9Y/u7vAMKD+v5DiACbO9FlWkGZO3/se6m8mNxSL
MwVsDT0v9e3iCwm7OcKni9f1b4QzDWOob2yAiEbu/btOEPqgmIidA+xmwusUlk/o
GcwaNvq64+/wG7DmMQIDAQAB<br />
-----END PUBLIC KEY-----<br />


Client requirements:<br />
1. Java 8: [download](http://www.oracle.com/technetwork/java/javase/downloads/jre8-downloads-2133155.html)
